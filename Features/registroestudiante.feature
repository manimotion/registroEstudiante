Feature: Login Feature
  El usuario quiere registrarse en el formulario de Registro Estudiantes.

  @smoketest
  Scenario: The user want to be a new estudent, they need to register in a form.
    Given The user enter in Registro Estudiante
    And Enter in the module Registrarse
    When Fill in all the form fields "1016063172","jaimito", "pruebas", "Calle123", "3108442253","j@i"
    Then the user can register in the platform.

  @regressiontest
  Scenario Outline: The user want to be a new estudent, they need to register in a form.
    Given The user enter in Registro Estudiante
    And Enter in the module Registrarse
    When Fill in all the form fields <usuario>,<nombre>, <apellido>, <direccion>, <telefono>,<email>
    Then the user can register in the platform.

    Examples: 
      | usuario   | nombre   | apellido   | direccion         | telefono     | email   |
      | "0000001" | "Jaime1" | "pruebas1" | "calle falsa 123" | "3120000001" | "ja@ja" |
