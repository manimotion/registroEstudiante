package interrapidisimo.devops.wpf;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverService;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {

	private static WiniumDriver driver;
	Properties loadProperty = new Properties();

	public Base(WiniumDriver driver) {
		Base.driver = driver;
	}

	public WiniumDriver winiumDriverConnection() throws IOException, InterruptedException {
		
		loadProperty.load(new FileReader("./params.properties"));

		DesktopOptions options = new DesktopOptions();
		options.setApplicationPath(loadProperty.getProperty("appDir"));

		File drivePath = new File(loadProperty.getProperty("winiumDriverDir"));

		WiniumDriverService service = new WiniumDriverService.Builder().usingDriverExecutable(drivePath).usingPort(9999).withVerbose(true).withSilent(false).buildDesktopService();
		service.start();
		driver = new WiniumDriver(service, options);

		return driver;
	}

	public WebElement findElement(By locator) {
		return driver.findElement(locator);
	}
	
	public static void Screenshoot() {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
	}


	public static void type(String inputText, By locator) {
		driver.findElement(locator).sendKeys(inputText);
	}

	public static void enter(By locator) {
		driver.findElement(locator).submit();
	}


	public static void closeDriver() throws IOException, InterruptedException {
		Process process = Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe");
		process.waitFor();
		process.destroy();
	}
	
	
	public List<WebElement> findElements(By Locator) {
		return driver.findElements(Locator);
	}
	
	public String getText (WebElement element) {
		return element.getText();
	}

	public String getText(By locator){
		return driver.findElement(locator).getText();
	}

	public void Sendkeys(By locator, String inputText){
		driver.findElement(locator).sendKeys(inputText);
	}

	public static void click(By locator){
		driver.findElement(locator).click();
	}

	public static boolean isDisplayed(By locator){
		try{
			return driver.findElement(locator).isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e){
			return false;
		}
	}

	public void getURL(String url){
		driver.get(url);
	}
	
	public void onMouse(By locator, By locator1) {
		WebElement ele = driver.findElement(locator);
		Actions act = new Actions(driver);
		act.moveToElement(ele).perform();
		driver.findElement(locator1).click();
	}

	public void moveMouseVertical(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
	}

	public void selectList(By locator){
		Select listSelect = new Select(driver.findElement(locator));
		listSelect.selectByIndex(1);
	}
	
	public static Timeouts EsperaImplicita(long tiempo){
		return driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);
	}
	
	public static void esperaExplicita(By locator, long tiempo ) {
		WebDriverWait myWaitVar = new WebDriverWait(driver,tiempo);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(locator));		
	}
	
	public void openNewTab(String Pagina) throws AWTException 
	{
		Robot r = new Robot();                          
		r.keyPress(KeyEvent.VK_CONTROL); 
		r.keyPress(KeyEvent.VK_T); 
		r.keyRelease(KeyEvent.VK_CONTROL); 
		r.keyRelease(KeyEvent.VK_T); 
		//Thread.sleep(3000);
		//ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		//driver.switchTo().window(tabs.get(1));
		//driver.get(Pagina);
		

	}
	public static void pegar(String texto, By locator) throws AWTException 
	{
		click(locator);
		StringSelection stringSelection = new StringSelection(texto);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		
		

	}
	public void Quit() {
		driver.quit();	
	}

	public void CambioPestaña(String esperado) {
		Set<String> tabs = driver.getWindowHandles();
		for(String tab: tabs) {
			driver.switchTo().window(tab);
			if(driver.getTitle().equals(esperado)) {
				break;
			}
			}
		}
		
	}