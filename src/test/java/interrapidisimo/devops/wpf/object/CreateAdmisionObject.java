package interrapidisimo.devops.wpf.object;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.winium.WiniumDriver;

import interrapidisimo.devops.wpf.Base;

public class CreateAdmisionObject extends Base {

	private static WiniumDriver driver;
	static Properties loadProperty = new Properties();

	// Localizadores
	static By btnRegistro = By.id("btnRegistro2");
	static By txtusuario = By.id("txt_usuario");
    static By txtNombre = By.id("txt_nombre");
    static By txtApellidos = By.id("txt_apellidos");
    static By txtDireccion = By.id("txt_direccion");
    static By txtTelefono = By.id("txt_telefono");
    static By txtEmail = By.id("txt_email");
    static By btnGuardar = By.id("btnGuardarRegistro");
    static By clickAceptar = By.id("2");
	
	

	public CreateAdmisionObject(WiniumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public static void abrirAplicacion() throws IOException, InterruptedException, AWTException {
		CreateAdmisionObject winDriver = new CreateAdmisionObject(driver);
		driver = winDriver.winiumDriverConnection();
		System.out.println("Abrio la app");
		Thread.sleep(5000);
	}

	public static void ingresarModulo() throws InterruptedException {
		// TODO Auto-generated method stub
		click(btnRegistro);
		Thread.sleep(5000);
	}
	
	public static void ingresarDatos(String usuario, String nombre, String apellido, String direccion, String telefono, String email) throws InterruptedException, IOException, AWTException {
		Thread.sleep(2000);
		click(txtusuario);
		type(usuario, txtusuario);
		type(nombre,txtNombre);
		type(apellido,txtApellidos);
		type(direccion,txtDireccion);
		type(telefono, txtTelefono);
		type(email, txtEmail);
		click(btnGuardar);
	}
		
	

	public static void validarPantalla() throws InterruptedException, IOException {
		assertTrue(isDisplayed(clickAceptar));
		click(clickAceptar);
		Thread.sleep(3000);
		//isDisplayed(validaDestinatario);
		System.out.println("Creo usuario");
		Screenshoot();
		driver.quit();
	}

}
