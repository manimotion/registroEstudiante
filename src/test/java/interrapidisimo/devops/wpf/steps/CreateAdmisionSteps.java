package interrapidisimo.devops.wpf.steps;

import java.awt.AWTException;
import java.io.IOException;

import interrapidisimo.devops.wpf.object.CreateAdmisionObject;

public class CreateAdmisionSteps {

	public static void abrirAplicacion() throws IOException, InterruptedException, AWTException {
		CreateAdmisionObject.abrirAplicacion();
	}

	
	public static void ingresarModuloAutomatico() throws InterruptedException, IOException {
		CreateAdmisionObject.ingresarModulo();
		
	}

	public static void ingresarDatos(String usuario, String nombre, String apellido, String direccion, String telefono, String email) throws InterruptedException, IOException, AWTException {
		CreateAdmisionObject.ingresarDatos(usuario,nombre, apellido, direccion, telefono, email);
		
	}

	public static void validarPantalla() throws InterruptedException, IOException {
		CreateAdmisionObject.validarPantalla();
		
	}

}
